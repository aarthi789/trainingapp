import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Comp1Component } from './component/comp1/comp1.component';
import { HomeComponent } from './component/home/home.component';
import { LoginComponent } from './component/login/login.component';

import { SignupComponent } from './component/signup/signup.component';
import { UsersComponent } from './component/users/users.component';
import { AuthGuardGuard } from './RouteAuthGuards/auth-guard.guard';

const routes: Routes = [

  {path:'', component:HomeComponent},
  {path:'signup', component:SignupComponent},
  {path:'login', component:LoginComponent},
  {path:'comp1', component:Comp1Component},
  {path:'users/:userName/:userId', component:UsersComponent, canActivate:[AuthGuardGuard]},
  {path:'products', loadChildren:()=>import ('./products/products/products.module').then(a=>a.ProductsModule)}
  
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
