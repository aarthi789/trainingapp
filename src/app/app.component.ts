import { Component } from '@angular/core';
import { Route, Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'routePractice';

  constructor(private router: Router){}
  navToComp1(){

    this.router.navigate(['/comp1']);
  }
}
