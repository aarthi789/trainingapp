import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './component/home/home.component';
import { SignupComponent } from './component/signup/signup.component';
import { LoginComponent } from './component/login/login.component';
import { NavComponent } from './component/nav/nav.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Comp1Component } from './component/comp1/comp1.component';
import { Comp2Component } from './component/comp2/comp2.component';
import { SamplePipe } from './sample.pipe';
import { PowerPipe } from './power.pipe';

import { UsersComponent } from './component/users/users.component';
import { AuthGuardGuard } from './RouteAuthGuards/auth-guard.guard';
import { FashionComponent } from './products/fashion/fashion.component';
import { HomeFurnitureComponent } from './products/home-furniture/home-furniture.component';
import { ElectronicsComponent } from './products/electronics/electronics.component';
import { BeautyComponent } from './products/beauty/beauty.component';
import { CustomeDirectiveDirective } from './custome-directive.directive';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SignupComponent,
    LoginComponent,
    NavComponent,
    Comp1Component,
    Comp2Component,
    SamplePipe,
    PowerPipe,
   
    UsersComponent,
        FashionComponent,
        HomeFurnitureComponent,
        ElectronicsComponent,
        BeautyComponent,
        CustomeDirectiveDirective,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule

  ],
  exports:[SamplePipe,PowerPipe],
  providers: [AuthGuardGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
