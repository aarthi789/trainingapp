import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-comp1',
  templateUrl: './comp1.component.html',
  styleUrls: ['./comp1.component.scss']
})
export class Comp1Component implements OnInit {

  fatherInfo={
    genes:"DNA",
    surname: "ammagari",
    qualities: "positive",
    money:"yes",
    assets: {
      home:"villa",
      money:"liquid",
      property:"land",
      vehicle:"car"
    }
  }
familyPictures=[
{
  name: 'arthi',
  pictureUrl:"https://images.pexels.com/photos/106399/pexels-photo-106399.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500",
},
{
  name: "rrr",
  pictureUrl:"https://media.gettyimages.com/photos/group-of-smiling-students-picture-id1166892023?k=20&m=1166892023&s=612x612&w=0&h=xJ_GQqRnJcxFG3y47rEY6BEBOV0o74Nq1DIRrjRz5KE=",

},
{
  name:'nagababu',
  pictureUrl:"https://media.gettyimages.com/photos/indian-school-children-in-classroom-picture-id1078112096?k=20&m=1078112096&s=612x612&w=0&h=aWYWIoYjiy55c64RxkG7UYlzMVlnoR6dePUp3EUCLn4=",
}
]
  constructor(private router:Router) { }

  ngOnInit(): void {
  }
  navToMain(){
    this.router.navigate(['']);
  }

  getReadyForRidewithBoy(data:any){
    console.log('Data from child component:::', data);
  }
}
