import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-comp2',
  templateUrl: './comp2.component.html',
  styleUrls: ['./comp2.component.scss']
})
export class Comp2Component implements OnInit,OnChanges {
  @Input() pictures:any;
  @Output() goForRide: EventEmitter<any>= new EventEmitter();

  salary= 4;
  math=10;
  exp=5;
  constructor() { }

  ngOnInit(): void {
    console.log(this.pictures)
    
  }

  ngOnChanges(changes: SimpleChanges): void {
      console.log(changes);
  }

 

  passingSignal(){
    const packingThing={
      clothes: "2pairs",
      foodItem: "pulihora",
      footwear: "2pairs",
      startTime: "2pm"
    }
    this.goForRide.emit(packingThing)
  }
}
