import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { BeautyService } from 'src/app/service/beauty.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnChanges {

  message = "This is from parent"
  webSitepictures: any = [];
  homeProducts:any=[];
  constructor(private beautyService: BeautyService) {

    this.webSitepictures = [
      {
        organigationName: 'hcl',
        eDate: new Date(),
        CEOName: 'Raghavendra',
        pictureUrl: "https://images.pexels.com/photos/106399/pexels-photo-106399.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500",
      },
      {
        organigationName: 'Tcs',
        eDate: new Date(),
        CEOName: 'arthi',
        pictureUrl: "https://media.gettyimages.com/photos/group-of-smiling-students-picture-id1166892023?k=20&m=1166892023&s=612x612&w=0&h=xJ_GQqRnJcxFG3y47rEY6BEBOV0o74Nq1DIRrjRz5KE=",

      },
      {
        organigationName: 'TECh M',
        eDate: new Date(),
        CEOName: 'arthi',
        pictureUrl: "https://media.gettyimages.com/photos/indian-school-children-in-classroom-picture-id1078112096?k=20&m=1078112096&s=612x612&w=0&h=aWYWIoYjiy55c64RxkG7UYlzMVlnoR6dePUp3EUCLn4=",
      }
    ];

    this.homeProducts=[
      {
        category:"Furniture",
        title:"Chair",
        description: "Take Wood",
        price:899,
        img:"https://m.media-amazon.com/images/I/41REIBCYW9L._AC_UL480_FMwebp_QL65_.jpg"
      },
      {
        category:"Furniture",
        title:"Table-mate",
        Description: "Take Wood",
        price:599,
        img:"https://m.media-amazon.com/images/I/61tbu79Rj2L._SX679_.jpg"
      },
      {
        category:"Furniture",
        title:"Shoe-Cabinate",
        Description: "Take Wood",
        price:900,
        img:"https://m.media-amazon.com/images/I/61VRxdsiIYL._SX522_.jpg"
      }
    ]
  }



  ngOnInit(): void {
    console.log("Loaded ngOnInit");

    this.beautyService.getSharingData().subscribe(data=>{
      console.log(data)
    })
  }

  ngOnChanges(changes:SimpleChanges): void{
    console.log("Loading ngOnChanges:", changes)
  }
 
}
