import { AfterContentChecked, AfterContentInit, AfterViewChecked, AfterViewInit, Component, OnChanges, OnDestroy, OnInit, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnChanges, OnDestroy, AfterContentInit, AfterContentChecked, AfterViewInit, AfterViewChecked{

  name="Aarthi";

  constructor(private router:Router) {
    console.log("This is constructor ")

   }

  ngOnInit(): void {
    console.log("This is ngOnInit ")
  }
  
  ngOnDestroy(): void {
    console.log("This is ngOnDestroy: ")
  }

  ngOnChanges(changes: SimpleChanges): void {
      console.log("This is Onchanges:" , changes)
  }

  ngAfterContentChecked(): void {
      console.log("This is from ngAfterContentChecked");
  }

  ngAfterContentInit(): void {
    console.log("This is from ngAfterContentInit");
  }

  ngAfterViewInit():void {
    console.log("This is from ngAfterViewInit");
  }

  ngAfterViewChecked(): void {
    console.log("This is from ngAfterViewChecked");
  }
  onSubmit(value:any){
    console.log(value)
  }

  navToSignup(){
    this.router.navigate(['/signup'])
  }
}
