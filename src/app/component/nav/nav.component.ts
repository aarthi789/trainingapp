import { Component, Input, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {

  name='Aarthi';
  id=123456;

  userInfo={
    userName:"Aarthi",
    contactNo:"123456789",
    address:"hyderabad"
  }

  constructor(private router:Router) { }

  ngOnInit(): void {
  }
  // @Input()  fromparent: string;
  navToUsers(){

    const navExtras: NavigationExtras={
      state:this.userInfo
    }
    this.router.navigate(['/users' + '/' + this.name + '/' + this.id], navExtras)
  }
}
