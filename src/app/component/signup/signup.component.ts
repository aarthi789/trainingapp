import { Component, DoCheck, Input, OnChanges, OnDestroy, OnInit, SimpleChanges } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, PatternValidator, Validators, FormBuilder} from '@angular/forms';
import { BeautyService } from 'src/app/service/beauty.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit, OnChanges, OnDestroy, DoCheck {
  @Input() loginName:String ='';

 
  constructor(private formBuilder: FormBuilder, private beautyService:BeautyService) {

    console.log("This is constructor")
   }

  // formName= this.formBuilder.group({
  //   firstName: ['', [Validators.required]],
  //   lastName: ['', [Validators.required]],
  //   email: ['', [ Validators.email, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$')] ],
  //   password: ['', [ Validators.required]],
  //   address:['', [Validators.required]],
  //   city:['', [Validators.required]],
  //   state:[''],
  //   zip:[''],
  //   gender:['', [Validators.required]]
  // })

  formName = this.formBuilder.group({
    firstName: ['', [Validators.required]],
    lastName: ['', [Validators.required]],
    email: ['',  [Validators.email, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$')] ],
    password: ['', [Validators.required]],
    address:['', [Validators.required]],
    city:['', [Validators.required]],
    state:['', [Validators.required]],
    zip:[''],
  });

  ngOnInit(): void {
    console.log("from ngOnInit:")
  }

 ngOnChanges(changes: SimpleChanges): void {
     console.log("From on changes:", changes )
 }

  ngOnDestroy(): void {
      console.log("From ngOn Destroy" )
  }

  ngDoCheck(): void {
      console.log("From Do check")
  }
  onSubmit(){
  
    console.log(this.formName.value );
    this.beautyService.setSharingData(this.formName.value)
  }

  get f() {
    return this.formName.controls
  }


}
