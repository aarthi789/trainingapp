import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BeautyService } from 'src/app/service/beauty.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  params:any;
  userInfo:any;

  headers=["id", "fullName", "contactNo","emailId", "Age", "gender", "country"];
  rows=[
    {
      id: "401",
      FullName:"Aarthi Ammagari",
      contactNo:"123456789",
      emailId:"aarthi@gmail.com",
      age:"25",
      gender:"Female",
      country:"India"

    },
    {
      id: "402",
      FullName:"Dharma Bharam",
      contactNo:"987654321",
      emailId:"dharma@gmail.com",
      age:"27",
      gender:"male",
      country:"Indian"

    },
    {
      id: "403",
      FullName:"Nagababu",
      contactNo:"963852741",
      emailId:"nagababu@yahoo.com",
      age:"27",
      gender:"male",
      country:"USA"

    },
    {
      id: "404",
      FullName:"Gopala Krishna",
      contactNo:"741852963",
      emailId:"gopal@gmail.com",
      age:"21",
      gender:"male",
      country:"Canada"

    },
    {
      id: "405",
      FullName:"Raghavendra",
      contactNo:"123789456",
      emailId:"raghava@gmail.com",
      age:"25",
      gender:"male",
      country:"Australia"

    }
  ]

  constructor(private router: Router,private activatedRoute : ActivatedRoute,  private beautyService: BeautyService) {

    this.activatedRoute.params.subscribe(param=>{
      console.log(param);
      this.params=param;
    })

    
    this.userInfo=this.router.getCurrentNavigation()?.extras.state;
    console.log(this.userInfo)
   }

  ngOnInit(): void {
    this.beautyService.getSharingData().subscribe(data=>{
      console.log(data)
    })
  }

}
