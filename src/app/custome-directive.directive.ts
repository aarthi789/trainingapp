import { Directive, HostBinding, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[appCustomeDirective]'
})
export class CustomeDirectiveDirective {

  @Input()
  defaultColor!: string;
  
  @Input()
  highlightColor!: string;

  @HostBinding('style.backgroundColor') backgroundColor!: string;

  constructor() { }
  @HostListener('mouseenter') mouseover(eventData:Event){
    this.backgroundColor=this.highlightColor;
  }

@HostListener('mouseleave')mouseleave(eventData:Event){
  this.backgroundColor=this.defaultColor
}
  
}
