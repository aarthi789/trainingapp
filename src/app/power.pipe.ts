import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  pure:true,
  name: 'power'
})
export class PowerPipe implements PipeTransform {

  transform(value: number, exp:number): number {
    let powerValue=Math.pow(value, exp)
    return  powerValue;
  }

}
