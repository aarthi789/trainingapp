import { Component, OnInit } from '@angular/core';
import { BeautyService } from 'src/app/service/beauty.service';

@Component({
  selector: 'app-beauty',
  templateUrl: './beauty.component.html',
  styleUrls: ['./beauty.component.scss']
})
export class BeautyComponent implements OnInit {

  makeup=[
    {
      id:1,
      item : "comb",
      MRP: '5rs',
      color:[
        'blue','red','white'
      ],
      size: '8cm',
      brandName: 'xyz',
      img:"https://m.media-amazon.com/images/I/71WmBY-nquL._SX522_.jpg"
    },
    {
      id:2,
      item:"lipstick",
      MRP:'700rps',
      color:['red','maroon', 'pink','black'],
      size: '8cm',
      brandName: 'sugar',
      img:"https://m.media-amazon.com/images/I/71kFOKp4pVL._SX522_PIbundle-4,TopRight,0,0_AA522SH20_.jpg"
    },
    {
      id:3,
      item:"Foundation",
      MRP:'1100rps',
      color:['nude','dark', 'whitesh','brown'],
      size: '8cm',
      brandName: 'colorbar',
      img:"https://m.media-amazon.com/images/I/61QXJ8q4HNL._AC_UL480_FMwebp_QL65_.jpg"
    },
    {
      id:4,
      item:"Maskara",
      MRP:'600rps',
      color:['black'],
      size: '8cm',
      brandName: 'Mabelline',
      img:"https://m.media-amazon.com/images/I/51YahIB3fiL._SX522_.jpg"
    },

  ]
  electronics=[
    {
      id:101,
      item : 'Hair Drier',
      MRP: '5000rps',
      color:[ 'white', 'black', 'purpule'],
      brandName:'Phylips',
      img:''
    },
    {
      id:102,
      item : 'Straightner',
      MRP: '10,000rps',
      color:[ 'white', 'blue', 'red'],
      brandName:'sony',
      img:''
    },
    {
      id:103,
      item : 'Trimmer',
      MRP: '1000rps',
      color:[ 'gray', 'white', 'Black'],
      brandName:'xyz',
      img:''
    },
    {
      id:104,
      item : 'Roller',
      MRP: '400rps',
      color:[ 'white', 'black', 'purpule'],
      brandName:'Phylips',
      img:''
    },
    {
      id:105,
      item : 'Roller',
      MRP: '400rps',
      color:[ 'white', 'black', 'purpule'],
      brandName:'Phylips',
      img:''
    },
  ];

  constructor(private beautyService: BeautyService) { }

  ngOnInit(): void {

    this.beautyService.setSharingData(this.electronics)
  }

}
