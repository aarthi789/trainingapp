import { Component, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-electronics',
  templateUrl: './electronics.component.html',
  styleUrls: ['./electronics.component.scss']
})
export class ElectronicsComponent implements OnInit {

  @Input() beautyElectronics:any;
  @Output() cellPhones:any;

  
  constructor() { }

  ngOnInit(): void {
    console.log(this.beautyElectronics)
  }

}
