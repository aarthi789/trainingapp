import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BeautyComponent } from '../beauty/beauty.component';
import { ElectronicsComponent } from '../electronics/electronics.component';
import { FashionComponent } from '../fashion/fashion.component';
import { HomeFurnitureComponent } from '../home-furniture/home-furniture.component';

const routes: Routes = [
  {path:'beauty', component:BeautyComponent},
  {path:'electronics', component:ElectronicsComponent},
  {path:'home-furniture', component:HomeFurnitureComponent},
  {path:'fashion', component:FashionComponent},
  {path: '', redirectTo:'/fashion'}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductsRoutingModule { }
