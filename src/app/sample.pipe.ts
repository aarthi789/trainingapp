import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'square'
})
export class SamplePipe implements PipeTransform {

  transform(value: number, ...args: unknown[]): unknown {
    return value*value;
  }
  
}
