import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BeautyService {

  sharingData= new Subject<any>()
  constructor() { }

  setSharingData(data:any){
    this.sharingData.next(data)
  }

  getSharingData(){
    return this.sharingData.asObservable()
    
  }
}
